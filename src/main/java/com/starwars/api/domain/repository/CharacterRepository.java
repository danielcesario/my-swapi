package com.starwars.api.domain.repository;

import com.starwars.api.domain.Character;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "characters", path = "characters")
public interface CharacterRepository extends CrudRepository<Character, Long> {

    List<Character> findByNameIgnoreCaseContaining(@Param("name") String name);

}
